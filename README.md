# Hangman
Gewinne oder stirb. Glaubst du du kennst deine Sprache? Rette dich vor dem Strick. \
Teste dein Wissen auf: https://daniels-page.web.app/Hangman/

<div align="center">
    <img src="resources/screenshot.png" alt="screenshot" height="300"/>
</div>

Note: Enable CORS for local developement to be able to load the words file.
