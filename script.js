var game

// Das Game-Objekt
class HangGame {
	constructor(word) {
		this.word = word;
		this.errors = 0;
		this.finished = false;
		this.field = "_" + " _".repeat(this.word.length - 1);
	}

	// Bearbeitet die Eingabe eines Buchstabens
	handle(chr) {
		if (this.finished) {
			return
		}

		if (this.check(chr)) {
			this.replace(chr)
			if (typeof this.field !== 'undefined' && !this.field.includes("_")) {
				this.finished = true
				runAway(0)
			}
		} else {
			this.errors += 1;
			this.draw()
			if (this.errors >= 6) {
				this.field = this.word.split("").join(" ")
				this.finished = true
			}
		}
	}

	// Testet einen Buchstaben
	check(chr) {
		if (this.word.includes(chr)) {
			return true
		} else {
			return false
		}
	}

	// Ersetzt alle _ durch Buchstaben falls dieser passt
	replace(chr) {
		for (let i = 0; i < this.word.length; i++) {
			if (this.word[i] == chr) {
				let field = this.field
				field = field.substr(0, i * 2) + chr + field.substr(i * 2 + 1);
				this.field = field;
			}
		}
	}

	// Zeichne die Körperteile
	draw() {
		if (this.errors == 1) {
			drawCircle(395, 135, 35, 10, "yellow", 1, 0.05, -0.05);
		} else if (this.errors == 2) {
			drawLine(395, 170, 395, 325, 10, "yellow", 0.05, -0.05);
		} else if (this.errors == 3) {
			drawLine(395, 220, 335, 170, 10, "yellow", 0.05, -0.05);
		} else if (this.errors == 4) {
			drawLine(395, 220, 455, 170, 10, "yellow", 0.05, -0.05);
		} else if (this.errors == 5) {
			drawLine(395, 320, 335, 380, 10, "yellow", 0.05, -0.05);
		} else if (this.errors == 6) {
			drawLine(395, 320, 455, 380, 10, "yellow", 0.05, -0.05, activateGif, [""]);
		}
	}
}

// Request AnimationFrame
(function () {
	var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
	window.requestAnimationFrame = requestAnimationFrame;
})();

// Zeichnet einen Kreis mit übergebenen Eigenschaften und ruft danach gegebenenfalls callback(args) auf
function drawCircle(x, y, radius, width, color, percent, speed, current, callback, args) {
	var canvas = document.getElementById("canvas");
	var context = canvas.getContext("2d");
	context.beginPath();
	context.strokeStyle = color;
	context.moveTo(x, y - radius);
	context.arc(x, y, radius, -(Math.PI / 2), ((Math.PI * 2) * current) - (Math.PI / 2), false);
	context.lineWidth = width;
	if (current >= 0) {
		context.stroke();
	}
	current = current + speed;
	if (current < (percent + speed)) {
		requestAnimationFrame(function () {
			drawCircle(x, y, radius, width, color, percent, speed, current, callback, args);
		});
	} else {
		if (callback && args) {
			callback.apply(this, args);
		}
	}
}

// Zeichnet eine Bezierkurve mit übergebenen Eigenschaften und ruft danach gegebenenfalls callback(args) auf
function drawBezier(x, y, a, b, i, j, k, l, width, color, speed, current, callback, args) {
	var canvas = document.getElementById("canvas");
	var context = canvas.getContext("2d");
	context.beginPath();
	context.strokeStyle = color;
	context.moveTo(((-x + 3 * i - 3 * k + a) * (current - speed) * (current - speed) * (current - speed)) + ((3 * x - 6 * i + 3 * k) * (current - speed) * (current - speed)) + ((-3 * x + 3 * i) * (current - speed)) + (x), ((-y + 3 * j - 3 * l + b) * (current - speed) * (current - speed) * (current - speed)) + ((3 * y - 6 * j + 3 * l) * (current - speed) * (current - speed)) + ((-3 * y + 3 * j) * (current - speed)) + (y));
	context.lineTo(((-x + 3 * i - 3 * k + a) * current * current * current) + ((3 * x - 6 * i + 3 * k) * current * current) + ((-3 * x + 3 * i) * current) + (x), ((-y + 3 * j - 3 * l + b) * current * current * current) + ((3 * y - 6 * j + 3 * l) * current * current) + ((-3 * y + 3 * j) * current) + (y));
	context.lineWidth = width;
	if (current >= 0) {
		context.stroke();
	}
	current = current + speed;
	if (current < (1 + speed)) {
		requestAnimationFrame(function () {
			drawBezier(x, y, a, b, i, j, k, l, width, color, speed, current, callback, args);
		});
	} else {
		if (callback && args) {
			callback.apply(this, args);
		}
	}
}

// Zeichnet eine Linie mit übergebenen Eigenschaften und ruft danach gegebenenfalls callback(args) auf
function drawLine(x, y, a, b, width, color, speed, current, callback, args) {
	var canvas = document.getElementById("canvas");
	var context = canvas.getContext("2d");
	context.beginPath();
	context.strokeStyle = color;
	context.moveTo(x, y);
	context.lineTo(x + (a - x) * current, y + (b - y) * current);
	context.lineWidth = width;
	if (current >= 0) {
		context.stroke();
	}
	current = current + speed;
	if (current < (1 + speed)) {
		requestAnimationFrame(function () {
			drawLine(x, y, a, b, width, color, speed, current, callback, args);
		});
	} else {
		if (callback && args) {
			callback.apply(this, args);
		}
	}
}

// Zeichnet den Galgen
function drawGallows() {
	var canvas = document.getElementById("canvas");
	var context = canvas.getContext("2d");
	context.clearRect(0, 0, canvas.width, canvas.height);
	drawBezier(10, 500, 250, 500, 35, 375, 225, 375, 20, "white", 0.06, -0.06);
	drawBezier(10, 500, 250, 500, 35, 375, 225, 375, 20, "white", 0.05, -0.05);
	drawBezier(10, 500, 250, 500, 35, 375, 225, 375, 20, "white", 0.04, -0.04,
		drawLine, [130, 410, 130, 0, 15, "white", 0.05, -0.05,
			drawLine, [130, 150, 61, 61, 10, "white", 0.10, -0.10,
				drawLine, [50, 50, 398, 50, 15, "white", 0.05, -0.05,
					drawLine, [395, 50, 395, 100, 5, "white", 0.10, -0.10]
				]
			]
		]
	);
}

// Lässt den Hangman am Galgen pendeln
function activateGif() {
	if (Math.random() > 0.9) {
		$("#gif").attr("src", "resources/winden.gif");
	} else {
		$("#gif").attr("src", "resources/swing.gif");
	}
	$("#gif").removeClass("invisible");
}

// Lässt den Hangman vom Galgen wegrennen
function runAway(x) {
	if (x == 0) {
		$("#gif").attr("src", "resources/black.png");
		$("#gif").removeClass("invisible");
		$("#run").removeClass("invisible");
	}
	$("#run").css("margin-left", 280 + x);
	$("#run").css("opacity", 1 - x / 900);
	if (x < 900) {
		requestAnimationFrame(function () {
			runAway(x + 5);
		});
	} else {
		$("#run").css("margin-left", 280);
		$("#run").addClass("invisible");
	}
}

// Lässt den Hangman an der Seite winken
function sayHello() {
	$("#hello img").attr("src", "resources/hello.gif");
	$("#hello").removeClass("invisible");
}

// Starte ein neues Spiel
function new_game(word) {
	game = new HangGame(word)

	$("#field").html(game.field);
	$(".letter").removeClass("clicked");
	$("#gif").addClass("invisible");
	drawGallows();
}

// Ein Buchstabe wurde gedrückt
$(".letter").click(function () {
	if (typeof game === 'undefined') {
		return;
	}

	$(this).addClass("clicked");

	if ($(".letter").length == $(".clicked").length) {
		sayHello();
	}

	var chr = $(this).html()
	game.handle(chr)
	$("#field").html(game.field);
});

// Erneutes Spiel wurde angefordert
$("#neu").click(function () {
	var word = get_new_word();
	new_game(word);
});

// Simuliere Mauseingaben auf die Buttons bei Tastatureingaben
$("body").keypress(function (input) {
	var key = input.which;
	if (key >= 97 && key <= 122) {
		key = key - 32;
		key = String.fromCharCode(key);
		if (!$("li:contains(" + key + ")").hasClass("clicked")) {
			$("li:contains(" + key + ")").trigger("click");
		}
	}
	if (key == 13) {
		$("#neu").trigger("click");
	}
});

// Wähle ein zufälliges Wort
function get_new_word() {
	var words = $("#words").text()
	words = words.split('\n');
	var word = words[Math.floor(Math.random() * words.length)]
	word = word.toUpperCase();
	return word
}

// Download der Woerter
$("#words").load("extern/derewo-woerter-bearbeitet.txt", function () {
	var word = get_new_word();
	new_game(word);
});